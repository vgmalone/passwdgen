# passwdGen

This python script creates a random password, 14 characters long consisting of:
- Upper and lower case letters
- At least 1 number
- 1 symbol
- ex: gOSAw!Nj3oyNTo
