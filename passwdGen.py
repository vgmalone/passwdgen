#!/usr/bin/python3

from random import choice
import random
from datetime import datetime
import base64

def randoDate():
    year = random.randrange(1988,2021)
    month = random.randrange(1,13) 
    day = random.randrange(0,31)
    hour = random.randrange(0,24)
    minute = random.randrange(0,60)
    second = random.randrange(0,60)

    random_date = datetime(year,month,day,hour,minute,second).ctime()
    return binaryConversion(random_date)


def binaryConversion(rdate):
    encoded_date = str(base64.b64encode(rdate.encode('ascii')))
    return selection(encoded_date[2:-1])


def selection(edate):
    start = random.randrange(2,19)
    sp = list(edate)
    simple_passwd = sp[start:start + 12]
    
    number = random.randrange(3,9)
    symbol = choice(['#','$','@','!','&','*'])

    i = random.randrange(1,10)
    j = random.randrange(2,11)

    simple_passwd.insert(i,str(number))
    simple_passwd.insert(j,symbol)

    print()
    # Takes ['1','h','e','@'...] and creates a string: 1he@...
    print(''.join(simple_passwd))
    print()

    return

#---------------------  Program Flow  -----------------------------------------

randoDate()
